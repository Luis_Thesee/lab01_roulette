import java.util.Scanner;
public class Roulette{
    public static void main (String[] args){

        RouletteWheel wheel = new RouletteWheel();
        Scanner sc = new Scanner(System.in);
        int money = 1000;

        System.out.println("would you like to bet? yes or no?");
        String answer = sc.nextLine();

        if(answer.equals("yes")){
            int bet = bet(money, sc);
                while(bet > money || bet < 0){
                    System.out.println("please re-enter a valid value");
                    bet = bet(money, sc);
                }
            int betNumber = bettingNumber(sc);
            int winnings= winnings(bet, betNumber, wheel);
            if(winnings > 0){
                money = money + winnings;
                System.out.println("CONGRATULATIONS YOU JUST WON: "+ winnings + "$");
                System.out.println("You now have "+ money + "$ in the bank");
            }
            else{
                money = money - bet;
                System.out.println("Better luck next time 😔");
                System.out.println("You now have "+ money + "$ in the bank");
            }
        }
        

    }

    public static int bet(int money, Scanner sc){
        System.out.println("How much money would you like to bet?");
        System.out.println("you currently have "+ money + "$ in the bank");
        return sc.nextInt();
    }

    public static int bettingNumber(Scanner sc){
        System.out.println("Which number would you like to bet on?");
        System.out.println("Please enter a number from 1-37");
        return sc.nextInt();
    }

    public static int winnings(int bet, int betNumber, RouletteWheel wheel){
        wheel.spin();
        if(betNumber == wheel.getValue()){
            return bet * 35;
        }
        else{
            return 0;
        }
    }
}