import java.util.Random;;
public class RouletteWheel {
    private int number;
    private Random random;

    public RouletteWheel() {
        this.random = new Random();
        this.number = 0;
    }

    public int getValue(){
        return this.number;
    }

    public void spin(){
        this.number = this.random.nextInt(37);
    }

}
